# Form implementation generated from reading ui file 'sampler.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!
"""
authors - Petra Čurdová, Matěj Jech
File implements GUI for searching our database.
"""

from PyQt5 import QtCore, QtGui, QtWidgets

def font_create(size, bold=False):
    """
    Creates a font
    :param size: size of the font in pixels
    :param bold: true if the font is bold, otherwise false
    :return: font: a font instance
    """
    f = QtGui.QFont()
    f.setPointSize(size)
    f.setBold(bold)
    f.setWeight(75)
    return f

def set_palette(objct, color):
    """
    Sets a color palette to the object
    :param objct: the object
    :param color: the color (RGB)
    """
    palette = QtGui.QPalette()
    brush = QtGui.QBrush(QtGui.QColor(*color))
    brush.setStyle(QtCore.Qt.SolidPattern)
    palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
    objct.setPalette(palette)

class UiMainWindow(object):
    """
    The UiMainWindow class represents a UI window with form.
    """
    def setup_ui(self, MainWindow):
        """
        Setups whole UI
        :param main_window: a window where the form will be set
        """
        MainWindow.setWindowTitle("Sampler")
        MainWindow.resize(771, 487)

        self.centralwidget = QtWidgets.QWidget(MainWindow)

        self.labels_list_create(font_create(10, True))
        self.push_button_create(font_create(10, True))
        self.check_box = self.check_box_create(480, 170, 151, 31, "Use optional", "Use optional parameters")
        self.combo_box_create()
        self.spin_box_create(150, 120, 91, 22, "iterations", 1, 2147483647, 1000, 1)
        self.double_spin_box_create(400, 120, 62, 22, "const", 0.001, 1.000, 0.005, 0.001)
        self.progress_bar_create()
        self.scroll_area_create()
        self.frame_create()

        self.frame.raise_()
        self.scroll_area.raise_()
        self.progress_bar.raise_()
        self.spin_box.raise_()
        self.double_spin_box.raise_()
        self.combo_box.raise_()
        self.push_button.raise_()
        self.check_box.raise_()

        for label in self.labels:
            label.raise_()

        MainWindow.setCentralWidget(self.centralwidget)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def labels_list_create(self, label_font):
        """
        Creates list of labels
        :param label_font: font for labels
        """
        self.labels = []
        self.labels.append(self.label_create(50, 30, 141, 41, label_font, "Database to sample"))
        self.labels.append(self.label_create(50, 80, 121, 31, label_font, "PARAMETERS"))
        label_font.setBold(False)
        self.labels.append(self.label_create(70, 120, 131, 21, label_font, "Iterations"))
        self.labels.append(self.label_create(270, 120, 131, 21, label_font, "Acceptance constant"))
        label_font.setPointSize(9)
        self.progress_label = self.label_create(40, 420, 71, 20, label_font, "Progress:")
        self.progress_label.setVisible(False)
        label_font.setPointSize(8)
        self.labels.append(self.label_create(140, 85, 211, 21, label_font, "(optional - default are ...)"))

    def label_create(self, x, y, width, height, label_font, text):
        """
        Creates a label
        :param x: x position of the label
        :param y: y position of the label
        :param width: label's width
        :param height: label's height
        :param label_font: font for label
        :param text: label's text
        :return: label: an instance of the created label
        """
        label = QtWidgets.QLabel(self.centralwidget)
        label.setGeometry(QtCore.QRect(x, y, width, height))
        label.setFont(label_font)
        label.setText(text)
        return label

    def push_button_create(self, button_font):
        """
        Creates a push button
        :param button_font: font of the text on the button
        """
        self.push_button = QtWidgets.QPushButton(self.centralwidget)
        self.push_button.setGeometry(QtCore.QRect(630, 170, 91, 31))
        self.push_button.setFont(button_font)
        self.push_button.setText("START")

    def check_box_create(self, x, y, width, height, name, text):
        """
        Creates a checkbox
        :param x: x position of the checkbox
        :param y: y position of the checkbox
        :param width: checkbox's width
        :param height: checkbox's height
        :param name: object name of the checkbox
        :param text: checkbox's text
        :return: checkbox: an instance of the created checkbox
        """
        checkbox = QtWidgets.QCheckBox(self.centralwidget)
        checkbox.setGeometry(QtCore.QRect(x, y, width, height))
        checkbox.setText(text)
        checkbox.setObjectName(name)
        return checkbox

    def combo_box_create(self):
        """
        Creates a comboboxes
        """
        self.combo_box = QtWidgets.QComboBox(self.centralwidget)
        self.combo_box.setGeometry(QtCore.QRect(210, 40, 141, 22))

    def progress_bar_create(self):
        self.progress_bar = QtWidgets.QProgressBar(self.centralwidget)
        self.progress_bar.setGeometry(QtCore.QRect(100, 420, 641, 23))
        self.progress_bar.setValue(0)
        self.progress_bar.setVisible(False)
        self.progress_bar.setStyleSheet(" QProgressBar { border: 1px solid grey; border-radius: 0px; text-align: center; }\
                                          QProgressBar::chunk {background-color: #3add36; width: 1px;}")

    def scroll_area_create(self):
        """
        Creates a scroll area
        """
        self.scroll_area = QtWidgets.QScrollArea(self.centralwidget)
        self.scroll_area.setGeometry(QtCore.QRect(30, 240, 711, 161))
        set_palette(self.scroll_area, (249, 249, 249))
        self.scroll_area.setAutoFillBackground(True)
        self.scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.scroll_area_widget_contents = QtWidgets.QWidget()
        self.scroll_area_contents_create()
        self.scroll_area.setWidget(self.scroll_area_widget_contents)

    def scroll_area_contents_create(self, size=8):
        """
        Creates contents for a scroll area
        :param size: size of a single label in pixels
        """
        #TO DO: change for sampler log
        self.vbox = QtWidgets.QVBoxLayout()
        for i in range(size):
            label = QtWidgets.QLabel("")
            scroll_font = font_create(8)
            scroll_font.setBold(False)
            label.setFont(scroll_font)
            label.setFixedWidth(self.scroll_area.width())
            self.vbox.addWidget(label)
        self.scroll_area_widget_contents.setLayout(self.vbox)
    
    def frame_create(self):
        """
        Creates a frame
        """
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(30, 20, 711, 201))
        set_palette(self.frame, (199, 199, 199))
        self.frame.setAutoFillBackground(True)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)

    def spin_box_create(self, x, y, width, height, name, min_value, max_value, step, value):
        """
        Creates a spinbox
        :param x: x position of the spinbox
        :param y: y position of the spinbox
        :param width: spinbox's width
        :param height: spinbox's height
        :param name: object name of the spinbox
        :param min_value: min value of the spinbox
        :param max_value: max value of the spinbox
        :param step: value of the step
        :param value: default vlaue of the spinbox
        """
        self.spin_box = QtWidgets.QSpinBox(self.centralwidget)
        self.spin_box.setGeometry(QtCore.QRect(x, y, width, height))
        self.spin_box.setObjectName(name)
        self.spin_box.setRange(min_value, max_value)
        self.spin_box.setSingleStep(step)
        self.spin_box.setValue(value)

    def double_spin_box_create(self, x, y, width, height, name, min_value, max_value, step, value):
        """
        Creates a double spinbox
        :param x: x position of the spinbox
        :param y: y position of the spinbox
        :param width: spinbox's width
        :param height: spinbox's height
        :param name: object name of the spinbox
        :param min_value: min value of the spinbox
        :param max_value: max value of the spinbox
        :param step: value of the step
        :param value: default vlaue of the spinbox
        """
        self.double_spin_box = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.double_spin_box.setGeometry(QtCore.QRect(x, y, width, height))
        self.double_spin_box.setObjectName(name)
        self.double_spin_box.setDecimals(3)
        self.double_spin_box.setRange(min_value, max_value)
        self.double_spin_box.setSingleStep(step)
        self.double_spin_box.setValue(value)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = UiMainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

