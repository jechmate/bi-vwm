CREATE TABLE IF NOT EXISTS Apartments (
                                        id integer PRIMARY KEY,
                                        Mesto text NOT NULL,
                                        Typ text NOT NULL,
                                        Rozloha integer NOT NULL,
                                        Konstrukce text NOT NULL,
                                        Podlazi integer NOT NULL,
                                        Parkovani text NOT NULL,
                                        Balkon text NOT NULL,
                                        Vytah text NOT NULL,
                                        Sklep text NOT NULL,
                                        Stav text NOT NULL,
                                        Cena integer NOT NULL
                                    );