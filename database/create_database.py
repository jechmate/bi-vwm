"""
authors - Petra Čurdová, Matěj Jech
File created using the https://www.sqlitetutorial.net/sqlite-python/ tutorial page
File generates a database into the /database folder. This database is used as the target for crawling dat.
"""
import database.insert_gen as ins_gn
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """
    Creates a new connection to a database in the db_file
    :param db_file: path to the database file
    :return: conn: the connection object
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def main(db, sql_create, sql_insert):
    """
    :param db: database filename
    :param sql_create: create script filename
    :param sql_insert: insert script filename
    """
    conn = create_connection(db)
    if conn is not None:
        # create cursor object
        cur = conn.cursor()
        drop = "DROP TABLE IF EXISTS Apartments"
        cur.execute(drop)
        # create apartments table
        with open(sql_create, 'r') as create_script:
            sql = create_script.read()
            create_table(conn, sql)
        # insert data from the insert_script file
        with open(sql_insert, 'r') as insert_script:
            data = insert_script.readlines()
            for line in data:
                cur.execute(line)
        conn.commit()
    else:
        print("Error! cannot create the database connection.")


if __name__ == '__main__':
    main("database/default.db", "database/create_table.sql", "database/insert_script.sql")
