"""
authors - Petra Čurdová, Matěj Jech
File generates a new insert script to the database folder.
"""
import random


class InsertGenerator:
    """
    Class that generates an insert script with random unique data
    """
    def __init__(self):
        self.apartments = set()
        self.cities = ['\'Praha\'', '\'Brno\'', '\'Ostrava\'', '\'Plzen\'', '\'Liberec\'', '\'Olomouc\'',
                       '\'Ceske Budejovice\'', '\'Usti nad Labem\'', '\'Hradec Kralove\'', '\'Pardubice\'',
                       '\'Zlin\'', '\'Havirov\'', '\'Kladno\'', '\'Most\'', '\'Opava\'', '\'Frydek-Mistek\'',
                       '\'Karvina\'', '\'Jihlava\'', '\'Teplice\'', '\'Decin\'', '\'Chomutov\'', '\'Karlovy Vary\'',
                       '\'Jablonec nad Nisou\'', '\'Mlada Boleslav\'', '\'Prostejov\'', '\'Prerov\'', '\'Ceska Lipa\'',
                       '\'Trebic\'', '\'Trinec\'', '\'Tabor\'']
        self.apt_type = ['\'1+kk\'', '\'1+1\'', '\'2+kk\'', '\'2+1\'', '\'3+kk\'',
                         '\'3+1\'', '\'4+kk\'', '\'4+1\'', '\'5+kk\'', '\'5+1\'']
        self.area = ['10', '20', '30', '40', '50', '60', '70', '80', '90', '100', '110',
                     '120', '130', '140', '150', '160', '170', '180', '190', '200']
        self.material = ['\'Cihlova\'', '\'Panelova\'', '\'Drevena\'', '\'Kamenna\'',
                         '\'Skeletova\'', '\'Montovana\'', '\'Smisena\'']
        self.floor = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        self.yes_no = ['\'ano\'', '\'ne\'']
        self.state = ['\'Novostavba\'', '\'Po rekonstrukci\'', '\'Velmi dobry\'',
                      '\'Dobry\'', '\'Spatny\'', '\'Pred rekonstrukci\'']
        self.price = ['400000', '500000', '600000', '700000', '800000', '900000', '1000000', '1100000', '1200000',
                      '1300000', '1400000', '1500000', '1600000', '1700000', '1800000', '1900000', '2000000', '2100000',
                      '2200000', '2300000', '2400000', '2500000', '2600000', '2700000', '2800000', '2900000', '3000000',
                      '3100000', '3200000', '3300000', '3400000', '3500000', '3600000', '3700000', '3800000', '3900000',
                      '4000000', '4100000', '4200000', '4300000', '4400000', '4500000', '4600000', '4700000', '4800000',
                      '4900000', '5000000', '5100000', '5200000', '5300000', '5400000', '5500000', '5600000', '5700000',
                      '5800000', '5900000', '6000000', '6100000', '6200000', '6300000', '6400000', '6500000', '6600000',
                      '6700000', '6800000', '6900000', '7000000', '7100000', '7200000', '7300000', '7400000', '7500000',
                      '7600000', '7700000', '7800000', '7900000', '8000000', '8100000', '8200000', '8300000', '8400000',
                      '8500000', '8600000', '8700000', '8800000', '8900000', '9000000', '9100000', '9200000', '9300000',
                      '9400000', '9500000', '9600000', '9700000', '9800000', '9900000', '10000000']
        self.prefix = \
            'insert into Apartments (id, Mesto, Typ, Rozloha, Konstrukce, Podlazi, '\
            'Parkovani, Balkon, Vytah, Sklep, Stav, Cena) values ('
        self.postfix = ');\n'

    def generate(self, database_size=10000):
        """
        Generates the insert script file
        :param database_size: number of rows to be inserted
        """
        insert_rows = 1
        with open('database/insert_script.sql', 'w') as script:
            while insert_rows < database_size + 1:
                statement = [str(insert_rows), random.choice(self.cities), random.choice(self.apt_type),
                             random.choice(self.area), random.choice(self.material), random.choice(self.floor),
                             random.choice(self.yes_no), random.choice(self.yes_no), random.choice(self.yes_no),
                             random.choice(self.yes_no), random.choice(self.state), random.choice(self.price)]
                insert_line = ', '.join(statement)
                if insert_line not in self.apartments:
                    self.apartments.add(self.prefix + insert_line + self.postfix)
                    insert_rows += 1
            for line in self.apartments:
                script.write(line)


if __name__ == "__main__":
    gen = InsertGenerator()
    gen.generate()
