"""
authors - Petra Čurdová, Matěj Jech
File contains the sampling algorithm that tries to reconstruct the target database.
"""
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import main_app
import random
import database.create_database as db_create

ITERATIONS_CNT = 1000


class Attribute:
    """
    A class which represents an attribute of a database table.

    ...

    Attributes
    -----------
    name : str
        a name of the attribute
    values: tuple
        a tuple of values that the attribute can acquire
    default_value: any
        a default value of the attribute
    selectable : boolean
        indicates if the attribute can be selected
    select_fnc : function
        a function which selects the attribute in form
    gui: MainUI
        a form allowing setting values to attributes

    Methods
    --------
    select_rand()
        selects a random value of the attribute
    reset()
        sets the attribute to its default value
    values_cnt()
        getter for possible values count
    """

    def __init__(self, name, values, def_val, select_fnc, gui=None):
        """
        Parameters
        -----------
        name : str
            a name of the attribute
        values: tuple
            a tuple of values that the attribute can acquire
        def_val: any
            a default value of the attribute
        select_fnc : function
            a function which selects the attribute in form
        gui: MainUI, optional
            a form allowing setting values to attributes (default is None)
        """

        self.name = name
        self.values = values
        self.default_value = def_val
        self.selectable = True
        self.select_fnc = select_fnc
        self.gui = gui

    def select_rand(self):
        """
        Selects a random value of the attribute.
        """

        if self.gui is not None:
            self.select_fnc(self.values[random.randint(0, len(self.values) - 1)], self.gui)
        else:
            self.select_fnc(self.values[random.randint(0, len(self.values) - 1)])

        if self.name != "Rozloha" and self.name != "Cena":
            self.selectable = False

    def reset(self):
        """
        Sets the value of the attribute to its default.
        """

        if self.gui is not None:
            self.select_fnc(self.default_value, self.gui)
        else:
            self.select_fnc(self.default_value)
        self.selectable = True

    def values_cnt(self) -> int():
        """
        Getter for possible values count.

        Returns possible values count
        """

        return len(self.values)


class RandomWalkSampler:
    """
    A class implementing a random walk sampler for sampling hidden databases.
    An algorithm inspired by: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.96.1181&rep=rep1&type=pdf

    ...

    Attributes
    -----------
    attributes: list
        a list of an attributes in a database table
    visible_limit: int
        maximal count of returned results from database
    const: float
        a constatnt which modifies the acceptance probability of the returned tuple from the database
    form: MainUI
        a form allowing access to the database
    execute_query: function
        a function which executes the specified query on the database
    get_result: function
        a function returning the result of the executed query

    Methods
    --------
    walk()
        implements the random walk algorithm
    reset_attributes()
        resets values of all attributes
    add_attribute(attr)
        appends a new attribute to the class
    save_result_to_file(result)
        saves the result of the sampling into a file
    """

    def __init__(self, limit, const, form, execute_query_fnc, get_result_fnc):
        """
        Parameters
        -----------
        limit: int
            maximal count of returned results from database
        const: float
            a constatnt which modifies the acceptance probability of the returned tuple from the database
        form: MainUI
            a form allowing access to the database
        execute_query_fnc: function
            a function which executes the specified query on the database
        get_result_fnc: function
            a function returning the result of the executed query
        """

        self.attributes = []
        self.results_set = set()
        self.visible_limit = limit
        self.const = const
        self.form = form
        self.execute_query = execute_query_fnc
        self.get_result = get_result_fnc

    def save_result_to_file(self, result):
        """
        Saves the result of the sampling into a file.

        Parameters
        -----------
        result: list of strings
            a result of the sampling to add into a file
        """

        # TO DO: save only unique results
        with open('sampling_result.txt', 'a') as file:
            for line in result:
                if line not in self.results_set:
                    file.write(line + '\n')
                    self.results_set.add(line)

    def walk(self):
        """
        Implements the random walk algorithm

        1. Takes a random selectable attribute.
        2. Gets a random value of current attribute and selects it.
        3. Executes query.
            - if overflow: goto 1.
            - elif underflow: reset_attributes()
            - else: computes probability and depending on it calls ave_result_to_file() or reset_attributes()
        """

        while True:
            selectable_attr = []
            product = 1.00
            for a in self.attributes:
                selectable_attr.append(a)

            while True:
                if len(selectable_attr) == 0:
                    self.reset_attributes()
                    break

                n = random.randint(0, len(selectable_attr) - 1)
                curr_attr = selectable_attr[n]
                curr_attr.select_rand()
                if not curr_attr.selectable:
                    selectable_attr.pop(n)

                product *= curr_attr.values_cnt()
                result_cnt = self.execute_query(self.form)
                if result_cnt > self.visible_limit:
                    continue
                elif result_cnt == 0:
                    self.reset_attributes()
                    break
                else:
                    if random.random() < min(self.const*product*result_cnt, 1):
                        self.save_result_to_file(self.get_result(self.form, result_cnt))
                        return
                    else:
                        self.reset_attributes()
                        break

    def reset_attributes(self):
        """
        Resets values of all attributes.
        """

        for a in self.attributes:
            a.reset()

    def add_attribute(self, attr):
        """
        Appends a new attribute to the class.

        Parameters
        -----------
        attr: Attribute
            an attribute to append
        """
        self.attributes.append(attr)


"""
Functions for simplifing interactions with GUI for the RandomWalkSampler.

...

floor_select_fnc(value, gui)
    depending on param value sets the value of spinbox and checks/unchecks the checkbox
state_select_fnc(value, gui)
    changes the value of the radiobuttons
area_select_fnc(value, gui)
    splits the range in two and selects one of them
price_select_fnc(value, gui)
    splits the range in two and selects one of them
execute(form)
    pushes the search button and executes the query 
"""


def floor_select_fnc(value, gui):
    """
    Depending on param value sets the value of spinbox and checks/unchecks the checkbox.

    Parameters
    -----------
    value: int or bool
        a value to set
    gui: MainUI
        a form allowing setting values to attributes
    """

    if not value:
        gui.checkbox_floor.setChecked(True)
    else:
        gui.checkbox_floor.setChecked(False)
        gui.spinboxes[0].setValue(value)


def state_select_fnc(value, gui):
    """
    Changes the value of the radiobuttons.

    Parameters
    -----------
    value: str
        a label of radiobutton to check
    gui: MainUI
        a form allowing setting values to attributes
    """

    for rb in gui.radiobuttons:
        if str(rb.objectName()) == value:
            rb.setChecked(True)
            break


def area_select_fnc(value, gui):
    """
    Splits the range in two and selects one of them.

    Parameters
    -----------
    value: int
        indicates which half will be selected
    gui: MainUI
        a form allowing setting values to attributes
    """

    if value == 0:
        gui.spinboxes[1].setValue(gui.spinboxes[1].minimum())
        gui.spinboxes[2].setValue(gui.spinboxes[2].maximum())
    elif value == 1:
        gui.spinboxes[2].setValue(int((gui.spinboxes[1].value() + gui.spinboxes[2].value())/2))
    else:
        gui.spinboxes[1].setValue(int((gui.spinboxes[1].value() + gui.spinboxes[2].value())/2))


def price_select_fnc(value, gui):
    """
    Splits the range in two and selects one of them.

    Parameters
    -----------
    value: int
        indicates which half will be selected
    gui: MainUI
        a form allowing setting values to attributes
    """

    if value == 0:
        gui.spinboxes[3].setValue(gui.spinboxes[3].minimum())
        gui.spinboxes[4].setValue(gui.spinboxes[4].maximum())
    elif value == 1:
        gui.spinboxes[4].setValue(int((gui.spinboxes[3].value() + gui.spinboxes[4].value())/2))
    else:
        gui.spinboxes[3].setValue(int((gui.spinboxes[3].value() + gui.spinboxes[4].value())/2))


def execute(form) -> int():
    """
    Pushes the search button and executes the query.

    Parameters
    -----------
    form: MainUI
        a form for executing query

    Returns number of results of the query.
    """

    form.pushButton.click()
    text = form.label_res_cnt.text()
    return int(text.split()[-1])


def get_res(form, cnt) -> []:
    """
    Getter for result of the query.

    Parameters
    -----------
    form: MainUI
        a form allowing access to the result
    cnt: int
        number of results to obtain
    Return list of the results.
    """

    return [result.objectName() for i, result in enumerate(form.scrollAreaWidgetContents.children()[1:]) if i < cnt]


def add_attributes(sampler, gui):
    """
    Adds attributes to the sampler.

    Parameters
    -----------
    sampler: RandomWalkSampler
        a sampler
    gui: MainUI
        a UI with attributes to add
    """

    sampler.add_attribute(Attribute("Mesto", ("Praha", "Trebic", "Most", "Liberec", "Usti nad Labem",
                                              "Ceske Budejovice", "Decin", "Hradec Kralove", "Ostrava",
                                              "Plzen", "Havirov", "Jablonec nad Nisou", "Pardubice", "Tabor", "Zlin",
                                              "Mlada Boleslav", "Ceska Lipa", "Prostejov", "Frydek-Mistek", "Brno"),
                                    "", gui.textEdit.setPlainText))

    sampler.add_attribute(Attribute("1+1",  (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "1+1"]))
    sampler.add_attribute(Attribute("1+kk", (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "1+kk"]))
    sampler.add_attribute(Attribute("2+1",  (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "2+1"]))
    sampler.add_attribute(Attribute("2+kk", (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "2+kk"]))
    sampler.add_attribute(Attribute("3+1",  (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "3+1"]))
    sampler.add_attribute(Attribute("3+kk", (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "3+kk"]))
    sampler.add_attribute(Attribute("4+1",  (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "4+1"]))
    sampler.add_attribute(Attribute("4+kk", (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "4+kk"]))
    sampler.add_attribute(Attribute("5+1",  (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "5+1"]))
    sampler.add_attribute(Attribute("5+kk", (False,), True,
                                    *[box.setChecked for box in gui.checkboxes_type if box.text() == "5+kk"]))

    sampler.add_attribute(Attribute("Konstrukce", ("Cihlova", "Smisena", "Montovana", "Kamenna",
                                                   "Skeletova", "Drevena", "Panelova"),
                                    "Nevybrano", gui.combo_box_construction.setCurrentText))

    sampler.add_attribute(Attribute("Parkovani", (True,), False, gui.checkboxes_equip[0].setChecked))
    sampler.add_attribute(Attribute("Balkon",    (True,), False, gui.checkboxes_equip[1].setChecked))
    sampler.add_attribute(Attribute("Vytah",     (True,), False, gui.checkboxes_equip[2].setChecked))
    sampler.add_attribute(Attribute("Sklep",     (True,), False, gui.checkboxes_equip[3].setChecked))

    sampler.add_attribute(Attribute("Podlazi", tuple(i for i in range(1, 11)), False, floor_select_fnc, gui))

    sampler.add_attribute(Attribute("Stav", ("Spatny", "Pred rekonstrukci", "Po rekonstrukci",
                                             "Dobry", "Velmi dobry", "Novostavba"),
                                    "Libovolny", state_select_fnc, gui))

    sampler.add_attribute(Attribute("Rozloha", (1, 2), 0, area_select_fnc,  gui))
    sampler.add_attribute(Attribute("Cena",    (1, 2), 0, price_select_fnc, gui))


def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', print_end="\r"):
    """
    Call in a loop to create terminal progress bar
    source: https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=print_end)
    # Print New Line on Complete
    if iteration == total: 
        print()


def string_to_insert(string, row_id) -> str:
    """
    Converts a string from the parameter to a valid SQL insert query
    :param string: the string to be converted
    :param row_id: the id of the row
    :return: sql insert line
    """
    line = ""
    prefix = \
        'insert into Apartments (id, Mesto, Typ, Rozloha, Konstrukce, Podlazi, ' \
        'Parkovani, Balkon, Vytah, Sklep, Stav, Cena) values ('
    postfix = ');\n'
    attr_list = string.split(", ")
    line = prefix + str(row_id) + ", '" + attr_list[0] + "', '" + attr_list[1] + "', " + str(attr_list[2]) + ", '" + \
           attr_list[3] + "', " + str(attr_list[4]) + ", '" + attr_list[5] + "', '" + attr_list[6] + "', '" + \
           attr_list[7] + "', '" + attr_list[8] + "', '" + attr_list[9] + "', " + str(attr_list[10][:-1]) + postfix
    return line


def generate_sampled_insert():
    """
    Generates an insert script from the sampled data
    """
    row_id = 1
    with open('database/sampled_insert.sql', 'w') as script:
        with open('sampling_result.txt', 'r') as source:
            lines = source.readlines()
            for line in lines:
                script.write(string_to_insert(line, row_id))
                row_id += 1


def main():
    # initialization
    app = QtWidgets.QApplication(sys.argv)
    gui = main_app.MainUI()
    gui.checkbox_floor.setChecked(True)
    for cb in gui.checkboxes_type:
        cb.setChecked(True)
    gui.pushButton.click()
        
    random.seed()                                   # setting seed for random sampler
    open('sampling_result.txt', 'w').close()        # cleaning the result file
    sampler = RandomWalkSampler(10, 0.2, gui, execute, get_res)
    add_attributes(sampler, gui)
    # **************

    # gui.show()
    # sampling process
    print_progress_bar(0, ITERATIONS_CNT, prefix='Sampling:', suffix='Complete', length=80)
    for i in range(ITERATIONS_CNT):
        sampler.walk()
        print_progress_bar(i + 1, ITERATIONS_CNT, prefix='Sampling:', suffix='Complete', length=80)
    # **************
    # sys.exit(app.exec_())
    generate_sampled_insert()
    db_create.main("database/sampled_database.db", "database/create_table.sql", "database/sampled_insert.sql")


if __name__ == "__main__":
    main()
