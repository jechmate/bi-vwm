"""
authors - Petra Čurdová, Matěj Jech
File implements form.py to create a working form for selecting data from our database.
"""
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import form
import sqlite3
from sqlite3 import Error
from os import listdir
from os.path import isfile, join


class MainUI(form.UiMainWindow, QtWidgets.QMainWindow):
    """
    The MainUI class represents the form window that can be interacted with. This window is also used in sampling.py
    for the random walk algorithm
    """
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent=parent)
        self.setup_ui(self)
        self.pushButton.clicked.connect(self.button_function)
        self.pushButtonNext.clicked.connect(self.next_page)
        self.pushButtonPrev.clicked.connect(self.prev_page)
        self.get_databases()
        self.connect_to_database()
        self.page = 1
        self.results = []

    def create_query(self):
        """
        Creates an SQL SELECT statement from the parameters selected in the form
        :return: string with the SQL statement
        """
        select_cmd = "SELECT * FROM Apartments WHERE"
        area = " Rozloha>=" + str(self.spinboxes[1].value()) + " AND Rozloha<=" + str(self.spinboxes[2].value())
        price = " AND Cena>=" + str(self.spinboxes[3].value()) + " AND Cena<=" + str(self.spinboxes[4].value())
        state = ""
        if not self.radiobuttons[0].isChecked():
            for radio_btn in self.radiobuttons:
                if radio_btn.isChecked():
                    state = " AND Stav=" + '\'' + str(radio_btn.objectName()) + '\''
                    break
        parking = ""
        if self.checkboxes_equip[0].isChecked():
            parking = " AND Parkovani=\'ano\'"
        balcony = ""
        if self.checkboxes_equip[1].isChecked():
            balcony = " AND Balkon=\'ano\'"
        elevator = ""
        if self.checkboxes_equip[2].isChecked():
            elevator = " AND Vytah=\'ano\'"
        basement = ""
        if self.checkboxes_equip[3].isChecked():
            basement = " AND Sklep=\'ano\'"
        floor = ""
        if not self.checkbox_floor.isChecked():
            floor = " AND Podlazi=" + str(self.spinboxes[0].value())
        construction = ""
        if self.combo_box_construction.currentText() != "Nevybrano":
            construction = " AND Konstrukce=" + '\'' + self.combo_box_construction.currentText() + '\''
        city = ""
        if self.textEdit.toPlainText() != "":
            city = " AND Mesto=" + '\'' + self.textEdit.toPlainText() + '\''
        apartment_type = ""
        selected_types = []
        for ap_type in self.checkboxes_type:
            if ap_type.isChecked():
                selected_types.append(str(ap_type.objectName()))
        if len(selected_types) > 0:
            if len(selected_types) == 1:
                apartment_type = " AND Typ=" + '\'' + selected_types[0] + '\''
            else:
                apartment_type = " AND ("
                for type_str in selected_types[:-1]:
                    apartment_type += "Typ=" + '\'' + type_str + '\'' + " OR "
                apartment_type += "Typ=" + '\'' + selected_types[-1] + '\'' + ")"

        select_cmd += area + price + state + parking + balcony + elevator + basement + \
                      floor + construction + city + apartment_type + " ORDER BY cena, Mesto, rozloha, typ ASC"
        return select_cmd

    def get_databases(self):
        """
        Adds availible databases to the dropdown menu
        :return:
        """
        files = [file for file in listdir("database/") if isfile(join("database/", file))]
        for file in files:
            if file[-3:] == ".db":
                self.combo_box_database.addItem(file[:-3])

    def connect_to_database(self):
        """
        Connects the app to a database
        :return:
        """
        try:
            self.conn = sqlite3.connect("database/" + self.combo_box_database.currentText() + ".db")
        except Error as e:
            print(e)
        self.cur = self.conn.cursor()

    def button_function(self):
        """
        The function that gets called after the button press.
        It creates an SQL statement by calling create_query, executes it and prints the top 10 results to the form
        :return:
        """
        self.connect_to_database()
        select_cmd = self.create_query()
        # print(select_cmd)
        self.cur.execute(select_cmd)
        self.results = self.cur.fetchall()
        showing_cnt = self.fill_scroll_area()
        self.label_res_cnt.setText("Zobrazuje se " + str(showing_cnt) + " výsledků z " + str(len(self.results)))

        if self.combo_box_database.currentText() == "default":
            self.show_pages(False)
        else:
            self.label_pages.setText("Stránka 1 z " + str(int((len(self.results) + 9) // 10)))
            self.show_pages(True)

    def fill_scroll_area(self, index=0):
        for label in self.scrollAreaWidgetContents.children()[1:]:
            label.setText("")
        cnt = 0
        for i, label in enumerate(self.scrollAreaWidgetContents.children()[1:]):
            if len(self.results) <= index*10 + i:
                break
            attr_list = str(self.results[index*10 + i][1:]).split(',')
            obj_name = ",".join(attr_list)[1:-1].replace('\'', "")
            line = ",".join(attr_list[:5])[1:].replace('\'', "")
            for j, attr in enumerate(attr_list[5:9]):
                if attr[2:-1] == "ano":
                    if j == 0:
                        line += ", parkování"
                    elif j == 1:
                        line += ", balkon"
                    elif j == 2:
                        line += ", výtah"
                    else:
                        line += ", sklep"
            line += "," + ",".join(attr_list[9:])[:-1].replace('\'', "") + " Kč"
            label.setText(line)
            label.setObjectName(obj_name)
            cnt += 1
        return cnt

    def show_pages(self, set_visible):   
        self.label_pages.setVisible(set_visible)
        self.pushButtonNext.setVisible(set_visible)
        self.pushButtonPrev.setVisible(False)
        self.page = 1

    def next_page(self):
        self.page += 1
        select_cmd = self.create_query()
        self.cur.execute(select_cmd)
        self.results = self.cur.fetchall()
        self.fill_scroll_area(self.page - 1)

        self.pushButtonPrev.setVisible(True)
        if self.page == int((len(self.results) + 9) // 10):
            self.pushButtonNext.setVisible(False)
        self.label_pages.setText("Stránka " + str(self.page) + " z " + str(int((len(self.results) + 9) // 10)))


    def prev_page(self):
        self.page -= 1
        select_cmd = self.create_query()
        self.cur.execute(select_cmd)
        self.results = self.cur.fetchall()
        self.fill_scroll_area(self.page - 1)

        self.pushButtonNext.setVisible(True)
        if self.page == 1:
            self.pushButtonPrev.setVisible(False)
        self.label_pages.setText("Stránka " + str(self.page) + " z " + str(int((len(self.results) + 9) // 10)))


def main():
    app = QtWidgets.QApplication(sys.argv)
    gui = MainUI()
    gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
