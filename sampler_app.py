"""
authors - Petra Čurdová, Matěj Jech
File implements GUI for searching our database.
"""
from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import sampler_view
import sampling
import main_app
import sqlite3
import random
from sqlite3 import Error
from os import listdir
from os.path import isfile, join
import database.create_database as db_create
import time

ITERATIONS_CNT = 1000
CONST = 0.2

class SamplerUI(sampler_view.UiMainWindow, QtWidgets.QMainWindow):
    """
    The SamplerUI class represents the sampler_view window that can be interacted with.
    """
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent=parent)
        self.setup_ui(self)
        self.push_button.clicked.connect(self.button_function)
        self.get_databases()
        self.connect_to_database()
        self.log_index = 0

    def get_databases(self):
        files = [file for file in listdir("database/") if isfile(join("database/", file))]
        for file in files:
            if file[-3:] == ".db":
                self.combo_box.addItem(file[:-3])

    def connect_to_database(self):
        try:
            self.conn = sqlite3.connect("database/" + self.combo_box.currentText() + ".db")
        except Error as e:
            print(e)
        self.cur = self.conn.cursor()
        self.cur.execute("SELECT * from Apartments")
        self.src_cnt = len(self.cur.fetchall())

    def button_function(self):
        """
        The function that gets called after the button press.
        """
        self.connect_to_database()
        if self.spin_box.value() >= 100 or (not self.check_box.isChecked() and ITERATIONS_CNT >= 100):
            self.progress_label.setVisible(True)
            self.progress_bar.setVisible(True)
            self.progress_bar.raise_()
        if self.check_box.isChecked():
            self.start_sampling(self.spin_box.value(), self.double_spin_box.value())
        else:
            self.start_sampling()

    def start_sampling(self, iterations=ITERATIONS_CNT, const=CONST):
        # initialization
        # app = QtWidgets.QApplication(sys.argv)
        self.clear_log()
        gui = main_app.MainUI()
        gui.checkbox_floor.setChecked(True)
        for cb in gui.checkboxes_type:
            cb.setChecked(True)
            
        gui.combo_box_database.setCurrentText(self.combo_box.currentText())     # selecting the right database
        random.seed()                                                           # setting seed for random sampler
        open('sampling_result.txt', 'w').close()                                # cleaning the result file
        sampler = sampling.RandomWalkSampler(10, const, gui, sampling.execute, sampling.get_res)
        sampling.add_attributes(sampler, gui)
        # **************
        # sampling process
        p = iterations // 100
        finished = 0
        self.log_write("Sampling started...")
        self.log_write("Iterations count: " + str(iterations))
        self.log_write("Acceptance constant: " + str(const))
        start = time.time()
        for i in range(iterations):
            sampler.walk()
            if iterations >= 100 and i % p == 0:
                self.progress_display(finished)
                finished += 1
            QtCore.QCoreApplication.processEvents()
        end = time.time()
        self.progress_display(100)
        # **************
        self.log_write("")
        self.log_write("FINISHED!")
        self.log_write("Reconstructed database saved in \"database/sampled_database.db\"")
        sampling.generate_sampled_insert()
        db_create.main("database/sampled_database.db", "database/create_table.sql", "database/sampled_insert.sql")
        self.print_result_cnt()
        self.print_time(end - start)

    def print_result_cnt(self):
        try:
            self.conn = sqlite3.connect("database/sampled_database.db")
        except Error as e:
            print(e)
        self.cur = self.conn.cursor()
        self.cur.execute("SELECT * from Apartments")
        sampled_cnt = len(self.cur.fetchall())
        self.log_write("Sampled " + str(sampled_cnt) + " rows out of " + str(self.src_cnt))

    def print_time(self, t):
        hrs = int(t // 3600)
        mins = int(t // 60 - hrs*60)
        secs = int(t - mins*60)
        if hrs != 0:
            self.log_write("Time: " + str(hrs) + " hours " + str(mins) + " minutes " + str(secs) + " seconds")
        elif mins != 0:
            self.log_write("Time: " + str(mins) + " minutes " + str(secs) + " seconds")
        else:
            self.log_write("Time: " + str(secs) + " seconds")

    def progress_display(self, num):
        self.progress_bar.setValue(num)

    def log_write(self, text):
        self.scroll_area_widget_contents.children()[1:][self.log_index].setText(text)
        self.log_index += 1

    def clear_log(self):
        for label in self.scroll_area_widget_contents.children()[1:]:
            label.setText("")
        self.log_index = 0


def main():
    app = QtWidgets.QApplication(sys.argv)
    gui = SamplerUI()
    gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()