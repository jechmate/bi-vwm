# Form implementation generated from reading ui file 'form.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!
"""
authors - Petra Čurdová, Matěj Jech
File implements GUI for searching our database.
"""
from PyQt5 import QtCore, QtGui, QtWidgets


def font_create(size, bold=False):
    """
    Creates a font
    :param size: size of the font in pixels
    :param bold: true if the font is bold, otherwise false
    :return: font: a font instance
    """
    f = QtGui.QFont()
    f.setPointSize(size)
    f.setBold(bold)
    f.setWeight(75)
    return f


def set_palette(objct, color):
    """
    Sets a color palette to the object
    :param objct: the object
    :param color: the color (RGB)
    """
    palette = QtGui.QPalette()
    brush = QtGui.QBrush(QtGui.QColor(*color))
    brush.setStyle(QtCore.Qt.SolidPattern)
    palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
    objct.setPalette(palette)


class UiMainWindow(object):
    """
    The Ui_MainWindow class represents a UI window with form.
    """
    def setup_ui(self, main_window):
        """
        Setups whole UI
        :param main_window: a window where the form will be set
        """
        main_window.setWindowTitle("Bytečky")
        main_window.resize(681, 738)
        set_palette(main_window, (226, 226, 226))

        self.centralwidget = QtWidgets.QWidget(main_window)
        self.centralwidget.setObjectName("centralwidget")

        self.text_edit_create()
        self.labels_list_create(font_create(10, True))
        self.combo_box_create()
        self.check_boxes_list_create()
        self.radio_buttons_list_create()
        self.spin_boxes_create()
        self.pushButton = self.push_button_create(510, 330, 121, 41, font_create(12, True), "HLEDAT")
        self.pushButtonPrev = self.push_button_create(475, 695, 25, 25, font_create(12), "«")
        self.pushButtonNext = self.push_button_create(510, 695, 25, 25, font_create(12), "»")
        self.pushButtonPrev.setVisible(False)
        self.pushButtonNext.setVisible(False)
        self.scroll_area_create()
        self.frame_create()

        self.frame.raise_()
        self.scrollArea.raise_()

        for label in self.labels:
            label.raise_()
        for checkbox in self.checkboxes_equip:
            checkbox.raise_()
        for checkbox in self.checkboxes_type:
            checkbox.raise_()
        self.checkbox_floor.raise_()
        for radiobutton in self.radiobuttons:
            radiobutton.raise_()
        for spinbox in self.spinboxes:
            spinbox.raise_()

        self.textEdit.raise_()
        self.label_res_cnt.raise_()
        self.label_pages.raise_()
        self.combo_box_construction.raise_()
        self.pushButton.raise_()
        self.pushButtonNext.raise_()
        self.pushButtonPrev.raise_()

        main_window.setCentralWidget(self.centralwidget)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def labels_list_create(self, label_font):
        """
        Creates list of labels
        :param label_font: font for labels
        """
        self.labels = []
        self.labels.append(self.label_create(40, 70, 55, 21, label_font, "Město"))
        self.labels.append(self.label_create(40, 110, 55, 21, label_font, "Typ"))
        self.labels.append(self.label_create(40, 180, 55, 21, label_font, "Rozloha"))
        self.labels.append(self.label_create(40, 220, 55, 21, label_font, "Cena"))
        self.labels.append(self.label_create(460, 70, 55, 21, label_font, "Stav"))
        self.labels.append(self.label_create(40, 260, 80, 21, label_font, "Konstrukce"))
        self.labels.append(self.label_create(40, 300, 60, 21, label_font, "Vybaveni"))
        self.labels.append(self.label_create(40, 340, 47, 21, label_font, "Podlazi"))
        self.labels.append(self.label_create(400, 15, 80, 21, label_font, "Databáze:"))
        label_font.setBold(False)
        label_font.setPointSize(8)
        self.labels.append(self.label_create(130, 180, 20, 21, label_font, "od:"))
        self.labels.append(self.label_create(250, 180, 20, 21, label_font, "do:"))
        self.labels.append(self.label_create(350, 180, 20, 21, label_font, "m"))
        self.labels.append(self.label_create(130, 220, 20, 21, label_font, "od:"))
        self.labels.append(self.label_create(250, 220, 20, 21, label_font, "do:"))
        self.labels.append(self.label_create(380, 220, 20, 21, label_font, "Kč"))
        self.label_res_cnt = self.label_create(30, 695, 200, 21, label_font, "Zobrazuje se 0 výsledků z 0")
        self.label_pages   = self.label_create(550, 695, 200, 21, label_font, "Stránka 0 z 0")
        self.label_pages.setVisible(False)
        label_font.setPointSize(6)
        self.labels.append(self.label_create(358, 176, 30, 21, label_font, "2"))
        label_font.setBold(True)
        label_font.setPointSize(12)
        self.labels.append(self.label_create(30, 15, 221, 21, label_font, "Vyhledávání bytů"))
        self.labels.append(self.label_create(30, 410, 91, 31, label_font, "Výsledky"))

    def label_create(self, x, y, width, height, label_font, text):
        """
        Creates a label
        :param x: x position of the label
        :param y: y position of the label
        :param width: label's width
        :param height: label's height
        :param label_font: font for label
        :param text: label's text
        :return: label: an instance of the created label
        """
        label = QtWidgets.QLabel(self.centralwidget)
        label.setGeometry(QtCore.QRect(x, y, width, height))
        label.setFont(label_font)
        label.setText(text)
        return label

    def combo_box_create(self):
        """
        Creates a comboboxes
        """
        self.combo_box_construction = QtWidgets.QComboBox(self.centralwidget)
        self.combo_box_construction.setGeometry(QtCore.QRect(130, 260, 161, 22))
        self.combo_box_construction.setCurrentText("Nevybráno")
        self.combo_box_construction.addItem("Nevybrano")
        self.combo_box_construction.addItem("Cihlova")
        self.combo_box_construction.addItem("Smisena")
        self.combo_box_construction.addItem("Montovana")
        self.combo_box_construction.addItem("Kamenna")
        self.combo_box_construction.addItem("Skeletova")
        self.combo_box_construction.addItem("Drevena")
        self.combo_box_construction.addItem("Panelova")

        self.combo_box_database = QtWidgets.QComboBox(self.centralwidget)
        self.combo_box_database.setGeometry(QtCore.QRect(480, 15, 161, 22))

    def check_boxes_list_create(self):
        """
        Creates lists of checkboxes
        """
        self.checkboxes_type = []
        self.checkboxes_equip = []
        self.checkbox_floor = self.check_box_create(185, 343, 70, 17, "Libovolne", "Libovolné")
        self.checkbox_floor.setChecked(True)
        self.checkboxes_equip.append(self.check_box_create(130, 300, 70, 17, "Parkovani", "Parkování"))
        self.checkboxes_equip.append(self.check_box_create(210, 300, 70, 17, "Balkon", "Balkon"))
        self.checkboxes_equip.append(self.check_box_create(290, 300, 70, 17, "Vytah", "Výtah"))
        self.checkboxes_equip.append(self.check_box_create(370, 300, 70, 17, "Sklep", "Sklep"))
        self.checkboxes_type.append(self.check_box_create(130, 110, 70, 17, "1+1", "1+1"))
        self.checkboxes_type.append(self.check_box_create(130, 140, 70, 17, "1+kk", "1+kk"))
        self.checkboxes_type.append(self.check_box_create(190, 110, 70, 17, "2+1", "2+1"))
        self.checkboxes_type.append(self.check_box_create(190, 140, 70, 17, "2+kk", "2+kk"))
        self.checkboxes_type.append(self.check_box_create(250, 110, 70, 17, "3+1", "3+1"))
        self.checkboxes_type.append(self.check_box_create(250, 140, 70, 17, "3+kk", "3+kk"))
        self.checkboxes_type.append(self.check_box_create(310, 110, 70, 17, "4+1", "4+1"))
        self.checkboxes_type.append(self.check_box_create(310, 140, 70, 17, "4+kk", "4+kk"))
        self.checkboxes_type.append(self.check_box_create(370, 110, 70, 17, "5+1", "5+1"))
        self.checkboxes_type.append(self.check_box_create(370, 140, 70, 17, "5+kk", "5+kk"))

    def check_box_create(self, x, y, width, height, name, text):
        """
        Creates a checkbox
        :param x: x position of the checkbox
        :param y: y position of the checkbox
        :param width: checkbox's width
        :param height: checkbox's height
        :param name: object name of the checkbox
        :param text: checkbox's text
        :return: checkbox: an instance of the created checkbox
        """
        checkbox = QtWidgets.QCheckBox(self.centralwidget)
        checkbox.setGeometry(QtCore.QRect(x, y, width, height))
        checkbox.setText(text)
        checkbox.setObjectName(name)
        return checkbox

    def radio_buttons_list_create(self):
        """
        Creates a list of radiobuttons
        """
        self.radiobuttons = []
        self.radio_button_create(510, 70, 82, 17, "Libovolny", "Libovolný")
        self.radio_button_create(510, 100, 82, 17, "Spatny", "Špatný")
        self.radio_button_create(510, 130, 121, 17, "Pred rekonstrukci", "Před rekonstrukcí")
        self.radio_button_create(510, 160, 101, 17, "Po rekonstrukci", "Po rekonstrukci")
        self.radio_button_create(510, 190, 82, 17, "Dobry", "Dobrý")
        self.radio_button_create(510, 220, 82, 17, "Velmi dobry", "Velmi dobrý")
        self.radio_button_create(510, 250, 82, 17, "Novostavba", "Novostavba")
        self.radiobuttons[0].setChecked(True)

    def radio_button_create(self, x, y, width, height, name, text):
        """
        Creates a radiobutton
        :param x: x position of the radiobutton
        :param y: y position of the radiobutton
        :param width: radiobuttons width
        :param height: radiobuttons height
        :param name: object name of the radiobutton
        :param text: radiobuttons text
        """
        self.radiobuttons.append(QtWidgets.QRadioButton(self.centralwidget))
        self.radiobuttons[-1].setGeometry(QtCore.QRect(x, y, width, height))
        self.radiobuttons[-1].setText(text)
        self.radiobuttons[-1].setObjectName(name)

    def text_edit_create(self):
        """
        Creates text edit widget
        """
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(130, 70, 291, 23))

    def spin_boxes_create(self):
        """
        Creates list of spinboxes
        """
        self.spinboxes = []
        self.spin_box_create(130, 340, 42, 22, "podlazi", 1, 10, 1, 1)
        self.spin_box_create(155, 180, 52, 22, "rozloha min", 0, 200, 5, 0)
        self.spin_box_create(290, 180, 52, 22, "rozloha max", 0, 200, 5, 200)
        self.spin_box_create(155, 220, 82, 22, "cena min", 0, 10_000_000, 100_000, 0)
        self.spin_box_create(290, 220, 82, 22, "cena max", 0, 10_000_000, 100_000, 10_000_000)

    def spin_box_create(self, x, y, width, height, name, min_value, max_value, step, value):
        """
        Creates a spinbox
        :param x: x position of the spinbox
        :param y: y position of the spinbox
        :param width: spinbox's width
        :param height: spinbox's height
        :param name: object name of the spinbox
        :param min_value: min value of the spinbox
        :param max_value: max value of the spinbox
        :param step: value of the step
        :param value: default vlaue of the spinbox
        """
        self.spinboxes.append(QtWidgets.QSpinBox(self.centralwidget))
        self.spinboxes[-1].setGeometry(QtCore.QRect(x, y, width, height))
        self.spinboxes[-1].setObjectName(name)
        self.spinboxes[-1].setRange(min_value, max_value)
        self.spinboxes[-1].setSingleStep(step)
        self.spinboxes[-1].setValue(value)

    def push_button_create(self, x, y, width, height, button_font, text):
        """
        Creates a push button
        :param x: x position of the button
        :param y: y position of the button
        :param width: button's width
        :param height: button's height
        :param button_font: font of the text on the button
        :param text: text on the button
        """
        btn = QtWidgets.QPushButton(self.centralwidget)
        btn.setGeometry(QtCore.QRect(x, y, width, height))
        btn.setFont(button_font)
        btn.setText(text)
        return btn

    def scroll_area_create(self):
        """
        Creates a scroll area
        """
        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setGeometry(QtCore.QRect(20, 450, 631, 241))
        set_palette(self.scrollArea, (248, 248, 248))
        self.scrollArea.setAutoFillBackground(True)
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scroll_area_contents_create()
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

    def scroll_area_contents_create(self, size=10):
        """
        Creates contents for a scroll area
        :param size: size of a single label in pixels
        """
        self.vbox = QtWidgets.QVBoxLayout()
        for i in range(size):
            label = QtWidgets.QLabel("")
            scroll_font = font_create(10)
            scroll_font.setBold(False)
            label.setFont(scroll_font)
            label.setFixedWidth(self.scrollArea.width())
            self.vbox.addWidget(label)
        self.scrollAreaWidgetContents.setLayout(self.vbox)

    def frame_create(self):
        """
        Creates a frame
        """
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(20, 49, 631, 341))
        set_palette(self.frame, (199, 199, 199))
        self.frame.setAutoFillBackground(True)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = UiMainWindow()
    ui.setup_ui(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
